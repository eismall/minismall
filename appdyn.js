var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var fs = require('fs');
var bodyParser = require('body-parser');
var MongoOplog = require('mongo-oplog');
var oplog = MongoOplog('mongodb://127.0.0.1:27017/local', 'gretajs');


/* chargement configuration JSON des actions --> controleurs */
global.actions_json = JSON.parse(fs.readFileSync("./routes/config_actions.json", 'utf8'));

global.schemas = {};

// Configuration de la connexion à la base de données via Mongoose :
var mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1:27017/gretajs', { useNewUrlParser: true }, function (err) {
  if (err) { throw err; } else console.log('Mongoose Connected');
});
// chargement des schémas depuis le fichier de configuration database_schemas.json
var database_schemas = JSON.parse(fs.readFileSync("database_schemas.json", 'utf8'));

for (modelName in database_schemas) {
  global.schemas[modelName] = mongoose.model(modelName,
    database_schemas[modelName].schema,
    database_schemas[modelName].collection);
}

var hbs = require('hbs');
// définir le répertoire de découpage de l'interface
hbs.registerPartials(__dirname + '/views/partials', function () {
  console.log('partials registered');
});

hbs.registerHelper('compare', function (lvalue, rvalue, options) {
  console.log("####### COMPARE lvalue :", lvalue, " et rvalue: ", rvalue);
  if (arguments.length < 3)
    throw new Error("Handlerbars Helper 'compare' needs 2 parameters");
  var operator = options.hash.operator || "==";
  var operators = {
    '==': function (l, r) {
      return l == r;
    },
    '<': function (l, r) {
      if ((typeof l == "string") && (typeof r == "string"))
        return parseInt(l) < parseInt(r);
      else return (l < r);
    },
    '>': function (l, r) {
      if ((typeof l == "string") && (typeof r == "string"))
        return parseInt(l) > parseInt(r);
      else return (l > r);

    },
    '<=': function (l, r) {
      if ((typeof l == "string") && (typeof r == "string"))
        return parseInt(l) <= parseInt(r);
      else return (l <= r);
    },
    '>=': function (l, r) {
      if ((typeof l == "string") && (typeof r == "string"))
        return parseInt(l) >= parseInt(r);
      else return (l >= r);
    }
  }
  if (!operators[operator])
    throw new Error("'compare' doesn't know the operator " + operator);
  var result = operators[operator](lvalue, rvalue);
  if (result) {
    return options.fn(this);
  } else {
    return options.inverse(this);
  }
});

// initialisation de la connexion à la base mongoDB
global.db = {};
var mongoClient = require('mongodb').MongoClient;

// Connexion URL
//var url = 'mongodb://simplon:azerty@127.0.0.1:27017/gretajs?authMechanism=DEFAULT';
var url = 'mongodb://127.0.0.1:27017/gretajs';  //remplacer simplonjs par gretajs
// Utilisation de la methode “connect” pour se connecter au serveur
mongoClient.connect(url, { useNewUrlParser: true }, function (err, client) {
  global.db = client.db('gretajs');  //On met en global la connexion à la base
  console.log("Connected successfully to server: global.db initialized");
});

// initialisation de la connexion à la base MySQL native
var mysql = require('mysql');
var config = { host: 'localhost', user: 'root', password: 'azerty', database: 'gretajs' };
// mysqlCnxvariable globale contenant l'accès à la base MySQL.
//global.mysqlCnx = mysql.createConnection(config);
//mysqlCnx.connect(function (err) {
//  if (err) { console.error('error connecting: ' + err.stack); return; }
//  console.log('connected as id ' + mysqlCnx.threadId);
//});

// Gestion oplog
console.log(oplog);
oplog.tail();

oplog.on('op', data => {
  console.log(data);
});

oplog.on('insert', doc => {
  console.log(doc);
});

oplog.on('update', doc => {
  console.log(doc);
});

oplog.on('delete', doc => {
  console.log(doc.o._id);
});

oplog.on('error', error => {
  console.log(error);
});

oplog.on('end', () => {
  console.log('Stream ended');
});

oplog.stop(() => {
  console.log('server stopped');
});


// initialisation de l'application Express
var app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json({ type: 'application/*+json' }))

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// initialisation du routeur dynamique
// Gestion des routes dynamiques via configuration json
require('./dynamicRouter')(app);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
