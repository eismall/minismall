var express = require('express');
var router = express.Router();
/* GET countriesSQL page. */
router.get('/', function (req, res, next) {
    var path = "/" + req.originalUrl.split('/')[1];
    var type = req.method;
        // Requête SQL via l’instance mysqlCnx
        global.mysqlCnx.query(global.actions_json[type + path].sql_query, function (error, results, fields) {
            if (error) throw error;
            console.log('records : ', results); // results is an array of records
            if (results.length == 0) results = null;
            if (global.actions_json[type+path].return_type == null) {
                res.render(global.actions_json[type + path].view, { title: 'List countries from SQL MySQL', country: results});
            } else {
                res.send(JSON.stringify(results));
            }    
        });
});
module.exports = router;