var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    var path = "/" + req.originalUrl.split('/')[1];
	var type = req.method;
    /*var readFile = require('fs').readFile;
    readFile('./countries.json', function(error, pays) { //lecture Asynchrone
        if (!error) {
            res.render('countries', {data: JSON.parse(pays)});
        } else { console.log('Erreur accès fichier : ', error) ;}
    });*/
    global.schemas[global.config_actions[type+path]].find({}, function(err, result) {
        if (err) {
          throw err;
        }
        console.log(result);
        res.render(global.actions_json[type+path].view, {stitle: 'First Cnx Mongo',
                            title: 'Liste déroulante', 
                            countries: result});
      });
});

module.exports = router;
