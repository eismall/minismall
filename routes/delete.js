var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

/* DELETE record from _id into url and into config_actions.json */
router.route('/:_id').get(function (req, res) {
    var path = "/" + req.originalUrl.split('/')[1];
    var type = req.method;
    // on peut utiliser une variable intermédiaire pour plus de lisibilité
    var model = global.actions_json[type + path].model;

    global.schemas[model].deleteOne({_id: new ObjectId(req.params._id)}, function (err, result) {
        if (err) {
            throw err;
        }
        global.schemas[model].find({}, function (err, result2) {
            console.log("result after delete : ", result2);
            res.render(global.actions_json[type + path].view, {
                title: "List of " + model,
                data: result2
            });
        });
    });
});

module.exports = router;