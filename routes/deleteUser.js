var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectID;

/* DELETE user from _id into url */
router.route('/:_id').get(function(req, res) {
  var path = "/" + req.originalUrl.split('/')[1];
	var type = req.method;
  console.log('req.originalUrl : ', req.originalUrl);
  global.db.collection('users').deleteOne({
    _id: new ObjectId(req.params._id)
  }, function(err, result) {
    if (err) {
      throw err;
    }
    global.db.collection('users').find({}, function(err, result2) {
      console.log("result into delteUser : ", result2);
      res.render(global.actions_json[type+path].view, {
        title: "List of users",
        users: result2 
      
      });
    });
  });
});

module.exports = router;
