/* ******************************************************
**  Module générique pour faire un                     **
**  "find({_id: new ObjectId(req.params._id)})"        **
**  filtrer avec l'_id                                 **
** ******************************************************/
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

/* GET users listing. */
router.route('/:_id').get(function (req, res, next) {
    var path = "/" + req.originalUrl.split('/')[1];
    var type = req.method;
    global.schemas[global.actions_json[type + path].model].find({ _id: new ObjectId(req.params._id)}, function (err, result) {
        if (err) {
            throw err;
        }
        console.log("findById : ", result[0]);
        if (result.length == 0) result = null;
        res.render(global.actions_json[type + path].view, { title: 'List of results', form_action: global.actions_json[type+path].form_action, data: result[0] });
    });
});


module.exports = router;