/* ******************************************************
**  Module générique pour faire un "find()" sans filtre *
** ******************************************************/
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

/* GET users listing. */
router.get('/', function (req, res, next) {
    var path = "/" + req.originalUrl.split('/')[1];
    var type = req.method;
    global.schemas[global.actions_json[type + path].model].find({}, function (err, result) {
        if (err) {
            throw err;
        }
        console.log(result);
        if (result.length == 0) result = null;
        if (global.actions_json[type+path].return_type == null) {
            res.render(global.actions_json[type + path].view, { title: 'List of results', form_action: global.actions_json[type+path].action_form,  data: result });
        } else {
            let json = result.map(function (p) {
                return p.toJSON()
            });
            console.log(json)
            res.send(json);
        }
    });
});

module.exports = router;