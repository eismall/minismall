var express = require('express');
var router = express.Router();

/* GET formUser page to insert a new user */
router.get('/', function(req, res, next) {
		var path = "/" + req.originalUrl.split('/')[1];
		var type = req.method;
    res.render(global.actions_json[type+path].view, {
                    title: global.actions_json[type+path].title , 
		            libelle: global.actions_json[type+path].libelle, 
		            form_action: global.actions_json[type+path].form_action
	});
});
module.exports = router;