var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    var path = "/" + req.originalUrl.split('/')[1];
    var type = req.method;
    // ici je fais la requête je récupère des valeur en json dans une variable
    var  monjson = { "nom":"MASCARON", "prenom": "stéphane"};
    res.render(global.actions_json[type+path].view, {data: monjson});
});

module.exports = router;
