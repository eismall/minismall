var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

/* Insert one new data into database via POST HTTP request */
router.route('/').post(function (req, res) {
    var path = req.originalUrl.split('?')[0];
    var type = req.method;
    var timeStamp = Math.floor(new Date().getTime() / 1000.0);
    console.log('req.originalUrl : ', req.originalUrl);
    req.body._id = new ObjectId();
    req.body.timestamp = timeStamp; // horodatage de l'insertion dans la base de données
    global.schemas[global.actions_json[type+path].model].create([req.body],
        function (err, result) {
            if (err) {
                throw err;
            }
            console.log('Inserted data : ', result);
            if (global.actions_json[type+path].return_type == null) {
                res.render(global.actions_json[type+path].view, { title: 'Data inserted into database :', 
                    data: result
                });
            } else {
                res.send(result); // renvoie les données insérée pour confirmation
            }
        } // fin callback de l'insert
    ); // fin de l'insert()
}); // fin de la gestion de cette route

router.route('/').get(function (req, res)  {
    var path = req.originalUrl.split('?')[0];
    var type = req.method;
    var timeStamp = Math.floor(new Date().getTime() / 1000.0);
    console.log('req.originalUrl : ', req.originalUrl);
    req.query._id = new ObjectId();
    req.query.timestamp = timeStamp; // horodatage de l'insertion dans la base de données
    global.schemas[global.actions_json[type+path].model].create([req.query],
        function (err, result) {
            if (err) {
                throw err;
            }
            console.log('Inserted data : ', result);
            if (global.actions_json[type+path].return_type == null) {
                res.render(global.actions_json[type+path].view, { title: 'Data inserted into database :', 
                    data: result
                });
            } else {
                result[0]._doc.msg = "data inserted";
                res.send(result); // renvoie les données insérée pour confirmation
            }
        } // fin callback de l'insert
    ); // fin de l'insert()
});
module.exports = router;