var express = require('express');
var router = express.Router();

/* GET formUser page to insert a new user */
router.get('/', function(req, res, next) {
		var path = "/" + req.originalUrl.split('/')[1];
		var type = req.method;
    res.render(global.actions_json[type+path].view, 
		{title: 'Create a new user', 
		 libelle: "creation", 
		 form_action: "/createUser"
	});
});
module.exports = router;