var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
/* SET data from _id with new data for an update into mongoDB . */
router.route('/:_id').get(function (req, res) {
	var path = "/" + req.originalUrl.split('/')[1];
    var type = req.method;
    var model = global.actions_json[type + path].model;

    global.schemas[model].update({_id: new ObjectId(req.params._id)}, {$set: req.query}, function (err, result) {
       	if (err) { throw err; }
         	console.log('result update module : ', result);
         	global.schemas[model].find({_id: new ObjectId(req.params._id)}, function (err, result) {
             	if (err) { throw err; }
             	console.log('data : ', result);
             	res.render(global.actions_json[type+path].view, {
                  	title: 'User modified without error',
                 	data: result[0]
             	});
         	 }); // fin du find() après update
       } // fin callback de l'update
    ); // fin de l'update()
}); // fin de la gestion de la route
module.exports = router;