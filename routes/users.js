var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

/* GET users listing. */
router.get('/', function(req, res, next) {
  var path = "/" + req.originalUrl.split('/')[1];
  var type = req.method;
  global.schemas[global.config_actions[type+path]].find({}, function(err,result){
    if(err){
      throw err;
    }
    console.log(result);
    if (result.length == 0) result = null;
    res.render(global.actions_json[type+path].view,{title:'List of users',users:result});
  });
});

module.exports = router;